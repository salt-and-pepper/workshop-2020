module.exports.role = () =>
  "arn:aws:iam::636214726664:role/prod_invoice-handler";

module.exports.deploymentBucket = () => "snp-tech-lambda-deployment";
module.exports.SQSUriInvoiceProcessor = () =>
  "https://sqs.eu-central-1.amazonaws.com/636214726664/prod-invoice-processor";
module.exports.SQSARNInvoiceProcessor = () =>
  "arn:aws:sqs:eu-central-1:636214726664:prod-invoice-processor";

module.exports.S3InvoiceBucket = () => "snp-tech-invoice-cache";
