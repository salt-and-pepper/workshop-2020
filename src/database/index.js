import Promise from "bluebird";
import faker from "faker";

const randomArraySize = ({ min = 0, max = 10 }) => [
  ...Array(faker.random.number({ min, max })).keys(),
];

export const Database = {
  pullData: () =>
    new Promise((resolve) =>
      setTimeout(
        () =>
          resolve(
            randomArraySize({ min: 1, max: 2 }).map(() => ({
              invoice: {
                number: `SNPRO${faker.random.number({
                  min: 100000,
                  max: 999999,
                })}`,
                paidToDate: faker.random.number({ min: 0, max: 100 }),
                subscriptions: randomArraySize({ min: 1, max: 5 }).map(() => {
                  const price = faker.commerce.price();
                  const quantity = faker.random.number({ min: 1, max: 5 });
                  return {
                    serviceName: faker.commerce.productName(),
                    price: faker.commerce.price(1, 150),
                    quantity,
                    total: (price * quantity).toFixed(2),
                  };
                }),
              },
              user: {
                uuid: faker.random.uuid(),
                firstName: faker.name.firstName(),
                lastName: faker.name.lastName(),
                email: faker.internet.email(),
                address: {
                  city: faker.address.city(),
                  country: faker.address.country(),
                  streetAddress: faker.address.streetAddress(),
                  zipCode: faker.address.zipCode("#####"),
                },
              },
            }))
          ),
        faker.random.number({ min: 1000, max: 10000 })
      )
    ),
};
