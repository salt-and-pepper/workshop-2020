import Promise from "bluebird";
import { SQS } from "aws-sdk";

import { Database } from "../database";

const sqs = new SQS();
const { SQS_INVOICE_PROCESSOR } = process.env;

export const main = async (ev, ctx, cb) => {
  ctx.callbackWaitsForEmptyEventLoop = false;

  const allParsedInvoices = await Database.pullData();

  await Promise.map(
    allParsedInvoices,
    (invoice) =>
      sqs
        .sendMessage({
          QueueUrl: SQS_INVOICE_PROCESSOR,
          MessageBody: JSON.stringify(invoice),
          DelaySeconds: 0,
        })
        .promise(),
    { concurrency: 100 }
  );

  return Promise.resolve();
};
