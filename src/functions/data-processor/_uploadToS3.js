import { S3 } from "aws-sdk";
import moment from "moment";
import { promises as fs } from "fs";

const s3 = new S3();
const { S3_INVOICE_BUCKET } = process.env;

export const uploadToS3 = async (filePath) => {
  const invoiceFS = await fs.readFile(filePath, "binary");

  const [emptySlot, uuid] = filePath.split(/^\/tmp\/invoice-(.*).pdf$/);
  console.log(2, { uuid });

  const params = {
    Bucket: S3_INVOICE_BUCKET,
    Key: `${uuid}/invoice-${moment().format("YYYYMMDD")}.pdf`,
    Body: new Buffer.from(invoiceFS, "binary"),
  };

  return s3.upload(params).promise();
};
