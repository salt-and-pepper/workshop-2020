import Promise from "bluebird";

import { renderInvoice } from "./invoice-generator";
import { uploadToS3 } from "./_uploadToS3";
import { EmailService } from "./_sendEmail";

export const main = async (ev, ctx, cb) => {
  ctx.callbackWaitsForEmptyEventLoop = false;

  const { Records } = ev;

  const processorProcedure = async ({ body }) => {
    const record = JSON.parse(body);
    const filePath = await renderInvoice(record);

    // TODO: DB SAVE
    await uploadToS3(filePath);
    await EmailService(filePath, record);
    // TODO: fs unlink
    return Promise.resolve();
  };

  await Promise.map(Records, processorProcedure, { concurrency: 2 });

  return Promise.resolve();
};
