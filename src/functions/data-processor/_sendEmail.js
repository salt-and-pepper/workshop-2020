import Promise from "bluebird";
import faker from "faker";

export const EmailService = () =>
  new Promise((resolve) =>
    setTimeout(() => resolve(), faker.random.number({ min: 50, max: 500 }))
  );
