export const TEXT_SIZE = 10;
export const FONT_BOLD = "Helvetica-Bold";
export const FONT_REGULAR = "Helvetica";

export const COLOR_BLACK = "#353535";
export const COLOR_LIGHT = "#cccccc";
export const COLOR_RED = "#e44257";
