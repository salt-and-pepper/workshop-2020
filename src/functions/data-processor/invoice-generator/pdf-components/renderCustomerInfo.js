import moment from "moment";

import { renderHR } from "./";
import {
  COLOR_RED,
  COLOR_BLACK,
  FONT_BOLD,
  FONT_REGULAR,
  TEXT_SIZE,
} from "../_constants";

export const renderCustomerInfo = (doc, invoiceInfo) => {
  const {
    invoice: { number: invoiceNumber, paidToDate, subscriptions },
    user: {
      firstName,
      lastName,
      address: { city, country, streetAddress, zipCode },
    },
  } = invoiceInfo;

  const subtotal = subscriptions.reduce(
    (acc, item) => (acc += parseFloat(item.total)),
    0
  );
  const total = subtotal - paidToDate;

  const customerRowSize = 20;
  const customerRow0 = 200;
  const customerRow1 = customerRow0 + customerRowSize;
  const customerRow2 = customerRow1 + customerRowSize;

  renderHR({ doc, y: customerRow0 - customerRowSize - 5 });

  doc
    .fillColor(COLOR_RED)
    .font(FONT_BOLD, TEXT_SIZE * 1.5)
    .text("Invoice", 50, customerRow0 - customerRowSize * 1.7);

  doc
    .fillColor(COLOR_BLACK)
    .font(FONT_REGULAR, TEXT_SIZE)
    .text("Invoice Number:", 50, customerRow0)
    .font(FONT_BOLD)
    .text(invoiceNumber, 150, customerRow0)
    .font(FONT_REGULAR)
    .text("Invoice Date:", 50, customerRow1)
    .text(moment().format("MM/DD/YYYY"), 150, customerRow1)
    // .font(FONT_BOLD)
    .fillColor(COLOR_RED)
    .text("Total Amount Due:", 50, customerRow2)
    .text(`$${total.toFixed(2)}`, 150, customerRow2)

    .fillColor(COLOR_BLACK)
    .text(`${firstName} ${lastName}`, 320, customerRow0)
    .font(FONT_REGULAR)
    .text(`${streetAddress}`, 320, customerRow1)
    .text(`${city}, ${zipCode}, ${country}`, 320, customerRow2)
    .moveDown();

  renderHR({ doc, y: customerRow2 + 5 });
};
