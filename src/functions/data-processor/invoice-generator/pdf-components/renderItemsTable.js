import moment from "moment";

import { renderHR } from "./";
import {
  COLOR_BLACK,
  COLOR_RED,
  FONT_BOLD,
  FONT_REGULAR,
  TEXT_SIZE,
} from "../_constants";

const interval = `${moment()
  .add("-1", "month")
  .format("MM/DD/YYYY")} - ${moment().format("MM/DD/YYYY")}`;
const tableTopPosition = 330;

const renderTableRow = ({
  doc,
  y,
  serviceName,
  billingPeriod,
  quantity,
  price,
  total,
  fontWeight = FONT_REGULAR,
  color = COLOR_BLACK,
}) =>
  doc
    .fillColor(color)
    .font(fontWeight, TEXT_SIZE)
    .text(serviceName, 50, y)
    .text(billingPeriod, 200, y)
    .text(price, 280, y, { width: 90, align: "right" })
    .text(quantity, 370, y, { width: 90, align: "right" })
    .text(total, 0, y, { align: "right" });

const renderTableHeader = (doc) => {
  renderTableRow({
    doc,
    y: tableTopPosition,
    serviceName: "Description",
    billingPeriod: "Billing period",
    quantity: "Quantity",
    price: "Unit Cost",
    total: "Subtotal (USD)",
    fontWeight: FONT_BOLD,
    color: COLOR_RED,
  });
  renderHR({ doc, y: tableTopPosition });
};

const renderTotal = (doc, invoiceInfo) => {
  const {
    invoice: { paidToDate, subscriptions },
  } = invoiceInfo;

  const subtotal = subscriptions.reduce(
    (acc, item) => (acc += parseFloat(item.total)),
    0
  );
  const total = subtotal - paidToDate;
  const firstTotalRowY = tableTopPosition + (subscriptions.length + 1) * 30;

  renderTableRow({
    doc,
    y: firstTotalRowY,
    quantity: "Subtotal",
    total: `$${subtotal.toFixed(2)}`,
  });

  renderTableRow({
    doc,
    y: firstTotalRowY + 15,
    quantity: "Paid To Date",
    total: `$${paidToDate.toFixed(2)}`,
  });

  renderTableRow({
    doc,
    y: firstTotalRowY + 30,
    quantity: "Total Amount Due*",
    total: `$${total.toFixed(2)}`,
  });
};

export const renderItemsTable = (doc, invoiceInfo) => {
  const {
    invoice: { subscriptions },
  } = invoiceInfo;

  renderTableHeader(doc);

  subscriptions.forEach((item, i) => {
    const y = tableTopPosition + (i + 1) * 30;

    renderTableRow({
      doc,
      y,
      ...item,
      billingPeriod: interval,
      price: `$${item.price}`,
      total: `$${item.total}`,
    });
    renderHR({ doc, y });
  });

  renderTotal(doc, invoiceInfo);
};
