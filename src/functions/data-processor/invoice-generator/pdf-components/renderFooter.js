import { COLOR_RED, TEXT_SIZE, FONT_REGULAR } from "../_constants";

export const renderFooter = (doc, code) =>
  doc
    .font(FONT_REGULAR, TEXT_SIZE)
    .fillColor(COLOR_RED)
    .text(
      "* Payment is due within 15 days.",
      0.5 * (doc.page.width - 100),
      doc.page.height - 90,
      {
        width: doc.page.width,
        align: "justify",
        lineBreak: false,
      }
    );
