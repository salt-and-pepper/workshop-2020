import { COLOR_LIGHT, TEXT_SIZE } from "../_constants";

export const renderHR = ({ doc, y }) =>
  doc
    .strokeColor(COLOR_LIGHT)
    .lineWidth(1)
    .moveTo(50, y + TEXT_SIZE * 1.4)
    .lineTo(550, y + TEXT_SIZE * 1.4)
    .stroke();
