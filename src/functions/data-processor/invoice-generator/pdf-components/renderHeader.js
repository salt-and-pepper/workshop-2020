import { COLOR_LIGHT, TEXT_SIZE, FONT_REGULAR } from "../_constants";

import { logoBase64 } from "./logo";

const logo = new Buffer.from(
  logoBase64.replace("data:image/png;base64,", ""),
  "base64"
);

export const renderHeader = (doc, code) =>
  doc
    .image(logo, 50, 45, { width: 120 })
    .fillColor(COLOR_LIGHT)
    .font(FONT_REGULAR, TEXT_SIZE)
    .text("2A Lunii Street", 200, 65, { align: "right" })
    .text("Cluj-Napoca, Cluj, Romania", 200, 80, { align: "right" })
    .moveDown();
