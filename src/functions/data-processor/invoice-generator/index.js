import Promise from "bluebird";
import PDFDocument from "pdfkit";
import fs from "fs";

import {
  renderCustomerInfo,
  renderFooter,
  renderHeader,
  renderItemsTable,
} from "./pdf-components";

export const renderInvoice = (invoiceInfo) =>
  new Promise((resolve, reject) => {
    const {
      user: { uuid },
    } = invoiceInfo;
    const fileName = `/tmp/invoice-${uuid}.pdf`;

    const doc = new PDFDocument();
    const fileWriteStream = fs.createWriteStream(fileName, { autoClose: true });
    doc.pipe(fileWriteStream);

    renderHeader(doc);
    renderCustomerInfo(doc, invoiceInfo);
    renderItemsTable(doc, invoiceInfo);
    renderFooter(doc);

    doc.end();

    fileWriteStream.on("error", (err) => reject(err));
    fileWriteStream.on("finish", () => resolve(fileName));
  });
