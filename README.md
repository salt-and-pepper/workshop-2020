# Salt & Pepper - Techsylvania Workshop 2020

## Beyond Borders

Presentation URi: [https://bit.ly/SnP-workshop-presentation-2020](https://bit.ly/SnP-workshop-presentation-2020)  
Workshop host: [Tudor Ghiorghiu](https://www.linkedin.com/in/tudorghiorghiu/)

## Requirements

- [Node 12+](https://nodejs.org/en/download/package-manager/)
- [Yarn](https://yarnpkg.com/getting-started/install)
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
- [Serverless framework](https://www.serverless.com/framework/docs/getting-started/)
- [AWS Account](https://aws.amazon.com/)

## Setup

Setting up the project is fairly straight forward. You just need to clone the repo and run

```
yarn
```

## Local development & debugging

For local development, consider using the already installed plugin, [serverless-offline](https://www.npmjs.com/package/serverless-offline)

```
sls offline -s STAGE_NAME
```

In the above example i've included the stage parameter (`-s`), but this can be set in the provider/function definition from the config file.
To understand how to setup the `serverless.yml` file, refer to the [AWS section in the serverless framework documentation](https://www.serverless.com/framework/docs/providers/aws/)

## Deploying

To deploy the entire application, you just need to run

```
sls deploy -s STAGE_NAME
```

Additionally, you can deploy only one function in order not to wait for the entire app to recompile.

```
sls deploy -s STAGE_NAME -f FUNCTION_NAME
```
